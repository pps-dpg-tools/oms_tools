#!/usr/bin/env python

# Common imports
from csv import field_size_limit
from pprint import pprint
import argparse
import os

# CMS imports
import runregistry

# My imports
from credentials import getCredentials
from pps_oms_interface import pps_oms_interface
from utils import *



class fillReport:
    run_variables = [
        'run_number', 'fill_number', 'start_time', 
        'end_time', 'duration', 'delivered_lumi', 'recorded_lumi'
    ]
    fill_variables = [
        'start_crossing_angle','stop_crossing_angle','beta_star_start',
        'beta_star_stop','start_stable_beam','end_stable_beam', 'era',
        'energy','peak_pileup','peak_lumi','efficiency_time','efficiency_lumi'
    ]
    out_field_names = [
        "Comment",
        "Fill",
        "Runs",
        "First run start time",
        "Last run end time",
        "Duration [h]",
        "Era",
        "Energy [GeV]",
        "Delivered luminosity [1/pb]",
        "Recorded luminosity [1/pb]",
        "Peak pileup",
        "Crossing angle",
        "Beta*",
        "Time efficiency [%]",
        "Lumi efficiency [%]",
        "Peak luminosity",
        "Stable beam start",
        "Stable beam end"
    ]
        
    def __init__(self, prevReportFile = ''):
        self.verbose = True
        self.report = []
        self.prevLastFill = 0
        self.prevLastRun = 0
        self.fillEntry = {}
        self.prevReportFile = prevReportFile
        if self.prevReportFile != '':
            try:
                with open(self.prevReportFile) as f_in:
                    self.prevLastFill, self.prevLastRun = self.parseInput(f_in)
            except FileNotFoundError:
                print('ERROR: previous fillReport ('+self.prevReportFile+') not found!')
                print('Continuing as if no previous fillReport was given...')
    
    def parseInput(self,file):
        """Parse CSV input file from previous fillReport tp determine last run and fill analyzed
        """
        # Get last line of file
        last_line = file.readlines()[-1]
        last_line = last_line.split(sep=',')
        if len(last_line) < len(self.out_field_names):
            print("WARNING: last line of previous fillReport did not contain the usual fields, ignoring it")
            return 0,0
        else:
            last_fill = int(last_line[1])
            runs = [int(run) for run in last_line[2].split(sep=' ')]
            last_run = max(runs)
            if self.verbose:
                print('Last run analyzed in the previous file:',last_run,'(fill '+str(last_fill)+')')
            return last_fill, last_run
            
    
    def printReport(self):
        pprint(self.report)
    
    def makeCSV(self,outputFileName='fillReport.csv'):
        with open(outputFileName,'w') as csv:
            # Add legends if this is the first csv
            if self.prevLastRun == 0:
                text_line = ''
                for field in self.out_field_names:
                    text_line += field + ','
                csv.write(text_line[:-1]+'\n')
                
            for entry in self.report:
                text_line = ''
                for field in entry:
                    if type(field) != list:
                        text_line += str(field)+','
                    else:
                        text_line += ' '.join(str(run) for run in field)+','
                
                csv.write(text_line[:-1]+'\n')
        print('Report saved as',outputFileName)
                        
                
    def initEntry(self):
        '''Initialize necessary entry variables
        '''
        self.fillEntry['current_fill'] = 0
        self.fillEntry['runList'] = []
        self.fillEntry['fill_rec_lumi'] = 0
        self.fillEntry['fill_del_lumi'] = 0
        self.fillEntry['fill_start_time'] = ''
        self.fillEntry['fill_end_time'] = ''
        self.fillEntry['duration'] = 0        
        self.fillEntry['finished'] = True

        
    def clearEntry(self):
        '''Clear entry variables
        '''
        self.fillEntry.clear()
        
    def insertEntry(self):
        '''Insert an entry in the fillReport
        '''
        
        # Format the crossing angle and beta star fields
        def startStopFieldLogic(startkey,stopKey):
            field = ''
            data = [self.fillEntry[startkey], self.fillEntry[stopKey]]
            if all([i is None for i in data]):
                field = 'Missing'
            elif any([i is None for i in data]) is None:
                if self.fillEntry[startkey] is None:
                    field = self.fillEntry[stopKey]
                else:
                    field = self.fillEntry[startkey]
            elif (self.fillEntry[startkey] == self.fillEntry[stopKey]):
                field = self.fillEntry[startkey]
            else:
                field = str(self.fillEntry[startkey])+' --> '+str(self.fillEntry[stopKey])
            return field
        
        # Define the fields order
        entryList = [
            '', # Field for comments
            self.fillEntry['current_fill'],
            self.fillEntry['runList'],
            self.fillEntry['fill_start_time'],
            self.fillEntry['fill_end_time'],
            self.fillEntry['duration'],
            self.fillEntry['era'],
            self.fillEntry['energy'],
            self.fillEntry['fill_del_lumi'],
            self.fillEntry['fill_rec_lumi'],
            self.fillEntry['peak_pileup'],
            startStopFieldLogic('start_crossing_angle','stop_crossing_angle'),
            startStopFieldLogic('beta_star_start','beta_star_stop'),
            self.fillEntry['efficiency_time'],
            self.fillEntry['efficiency_lumi'],
            self.fillEntry['peak_lumi'],
            self.fillEntry['start_stable_beam'],
            self.fillEntry['end_stable_beam'],
        ]
        # Format the entry
        self.report.append(entryList)
        
    def computeEntry(self):
        '''Compute entry variables once all the ones obtained from DB are filled
        '''
        # Compute fill duration in hours
        self.fillEntry['duration'] = (self.fillEntry['d_fill_end_time']-self.fillEntry['d_fill_start_time']).seconds/3600
            
    def getRunsToProcess(self):
        """Get the runs to be processed from runregistry
        """
        # Set certificate variable for runregistry to work
        os.environ["CERN_CERTIFICATE_PATH"] = "/eos/project-c/ctpps/subsystems/Automation/Certificates/"
        
        # Get all Run 3 runs with run number higher than the one from the previous fillReport
        runs = runregistry.get_runs(
            filter = {
                'end_time' : { 'and': [
                        {'>' : '2022-01-01T00:00:00'},
                        {'<>' : 'None'}
                    ]    
                },
                'run_number': {'>' : self.prevLastRun},
                'class' : { 'like': '%Collisions%'}
            }
        )
        
        def get_from_oms_or_false(attr,run):
            '''Needed because keys are not always present'''
            try:
                return run['oms_attributes'][attr]
            except KeyError:
                return False

        # Require either CTPPS or CTPPS_TOT to be included
        runs = [
                run for run in runs 
                if get_from_oms_or_false('ctpps_included',run) or 
                get_from_oms_or_false('ctpps_tot_included',run)
            ]
        
        return runs
                    
    def processRuns(self,runs):
        '''
        Sort runs creating the usual fillReport style and retrieve infos from OMS. 
        Assume runs in reversed order.
        '''
        if len(runs) == 0:
            print('WARNING: no runs to process have been found')
            return
        
        pps_oms = pps_oms_interface()
        
        self.initEntry()
        for run in reversed(runs):
            if self.verbose:
                print('Processing run:',run['run_number'])
                
            # Get info about the run from OMS
            runInfo = pps_oms.getRunInfo(run['run_number'],self.run_variables)

            if run['oms_attributes']['fill_number'] != self.fillEntry['current_fill']:
                # Add entry every time a fill is completed
                if self.fillEntry['current_fill'] != 0:
                    self.computeEntry()
                    if self.fillEntry['finished']:
                        self.insertEntry()
        
                self.clearEntry()
                self.initEntry()
                self.fillEntry['current_fill'] = run['oms_attributes']['fill_number']
                self.fillEntry['d_fill_start_time'], self.fillEntry['fill_start_time'] = formatDate(run['oms_attributes']['start_time'])   
                
                if self.verbose:
                    print('New fill found:',self.fillEntry['current_fill'])
                
                # Retrieve and fill fill variables
                fillInfo = pps_oms.getFillInfo(self.fillEntry['current_fill'],self.fill_variables)
                for key in fillInfo.keys():
                    if key == 'start_stable_beam' or key == 'end_stable_beam':
                        # Special parsing for datetime variables
                        if fillInfo[key] is None:
                            print('WARNING: fill',self.fillEntry['current_fill'],' is not over yet, skipping it...')
                            self.fillEntry['finished'] = False
                        else:
                            _,self.fillEntry[key] = formatDate(fillInfo[key])
                    else:
                        self.fillEntry[key] = fillInfo[key]
                
            self.fillEntry['runList'].append(run['run_number'])
            self.fillEntry['d_fill_end_time'], self.fillEntry['fill_end_time'] = formatDate(run['oms_attributes']['end_time'])
            # Aggregate luminosity
            self.fillEntry['fill_rec_lumi'] += runInfo['recorded_lumi']
            self.fillEntry['fill_del_lumi'] += runInfo['delivered_lumi']
                        
        # Add the last fill
        self.computeEntry()
        if self.fillEntry['finished']:
            self.insertEntry()

def parseArgs():
    parser=argparse.ArgumentParser(
        description='This scripts generates the usual PPS-style fillReport',
        epilog='Made by A. Bellora, 03/08/2022'
    )
    parser.add_argument('-i','--inputFile', type=str, help='Path to the previous fillReport file',default='')
    parser.add_argument('-o','--outputFile', type=str, help='Path for the new fillReport file',default='fillReport.csv')
    args = parser.parse_args()
    return args
    
if __name__ == '__main__':
    args = parseArgs()
    report = fillReport(args.inputFile)
    runs = report.getRunsToProcess()
    report.processRuns(runs)
    report.makeCSV(args.outputFile)