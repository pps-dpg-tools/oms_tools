# OMS_Tools

PPS tools to interface with OMS

## Setup

To setup a project area follow these steps:
1. Clone this repository:
   ```
   git clone https://gitlab.cern.ch/pps-dpg-tools/oms_tools.git
   ```
2. Install the [Run registry API][2]:
   ```
   virtualenv -p `which python3` venv
   source venv/bin/activate
   pip install runregistry
   ```
3. Install the [CMS OMS API][1]:
   ```
   git clone ssh://git@gitlab.cern.ch:7999/cmsoms/oms-api-client.git
   cd oms-api-client
   python3 -m pip install -r requirements.txt
   python3 setup.py install
   cd ..
   ``` 
4. Install additional packages:
   ```
   pip install pytz
   ```
5. Edit credentials.txt, substituting `user password` with the designated credentials (ask me if you don't have an OpenID application allowed to access OMS)

## Usage
### FillReport
1. Activate the virtual environment if you have not done it already:
   ```
   source venv/bin/activate
   ```
2. Make fillReport.py executable:
   ```
   chmod u+x fillReport.py
   ```
3. Execute fillReport.py (you can check the optional parameters using the -h/--help option)
   ```
   optional arguments:
     -h, --help         show this help message and exit
     -i INPUTFILE, --inputFile INPUTFILE
                        Path to the previous fillReport file
     -o OUTPUTFILE, --outputFile OUTPUTFILE
                        Path for the new fillReport file
   ```


[1]: https://gitlab.cern.ch/cmsoms/oms-api-client/-/tree/master/
[2]: https://github.com/cms-DQM/runregistry/tree/master/runregistry_api_client 
