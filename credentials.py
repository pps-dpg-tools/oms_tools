import sys

def getCredentials():
    try:
        with open('credentials.txt') as f:
            creds = []
            for line in f.readlines():
                if line.startswith('#'):
                    continue
                else:
                    creds = line.split()
            if len(creds) != 2:
                print('ERROR - Credentials not recognized in credentials.txt')
                sys.exit(1)
            else:
                usr,pwd = creds
        return usr,pwd
    except FileNotFoundError:
        print('ERROR - Credential file not found: you should create a credentials.txt file with your user and password')
        sys.exit(1)

if __name__ == '__main__':
    usr,pwd = getCredentials()
    print('Your user is:',usr)
    print('Your password is:',pwd)