import datetime 
import pytz
import sys

def formatDate(date):
    format_in = '%Y-%m-%dT%H:%M:%SZ'
    d = datetime.datetime.strptime(date,format_in).replace(tzinfo=pytz.utc)
    format_out = '%Y-%m-%d %H:%M:%S'
    return d, d.astimezone(pytz.timezone('Europe/Zurich')).strftime(format_out)

if __name__ == '__main__':
    print('This is a utilities class, not foreseen to be executed by itself')
    sys.exit(1)