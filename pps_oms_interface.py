from omsapi import OMSAPI
from credentials import getCredentials

from pprint import pprint

class pps_oms_interface:
    """Class to handle the OMS queries from the PPS tools
    """
    def __init__(self):
        usr,pwd = getCredentials()
        self.oms = OMSAPI("https://cmsoms.cern.ch/agg/api", "v1", cert_verify=False, verbose=False)
        self.oms.auth_oidc(usr,pwd)
        
    def printTableAttributes(self,table,filter=''):
        q = self.oms.query(table)
        if filter !=  '':
            q.filters(filter)
        print(*list(q.data().json()['data'][-1]['attributes'].keys()),sep='\n')
        
    def getRunInfo(self,run_number: int,attributes: list) -> dict:
        q = self.oms.query("runs")
        q.filter('run_number',run_number)
        q.attrs(attributes)
        return q.data().json()['data'][-1]['attributes']
    
    def getFillInfo(self,fill_number: int,attributes: list) -> dict:
        q = self.oms.query("fills")
        q.filter('fill_number',fill_number)
        q.attrs(attributes)
        return q.data().json()['data'][-1]['attributes']
        
if __name__ == '__main__':
    oms = pps_oms_interface()
    print('Fill variables:')
    oms.printTableAttributes(
        'fills',
        [
            {'attribute_name' : 'fill_number',
            'value' : 8081,
            'operator' : 'EQ'}
        ]
    )
    print('Run variables:')
    oms.printTableAttributes(
        'runs',
        [
            {'attribute_name' : 'run_number',
            'value' : 356615,
            'operator' : 'EQ'}
        ]
    )
     